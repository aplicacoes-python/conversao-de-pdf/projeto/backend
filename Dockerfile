FROM python:3.9

# Defina o diretório de trabalho no contêiner
WORKDIR /app

# Copie os arquivos do projeto para o diretório de trabalho no contêiner
COPY . /app

# Instale as dependências do projeto
RUN pip install --no-cache-dir PyPDF2