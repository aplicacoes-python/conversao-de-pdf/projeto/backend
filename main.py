import base64

def pdf_to_base64(pdf_path):
    with open(pdf_path, "rb") as f:
        pdf_bytes = f.read()
        encoded_pdf = base64.b64encode(pdf_bytes)
        return encoded_pdf.decode('utf-8')  # Retorna a representação em base64 como uma string

# Caminho para o arquivo PDF
pdf_path = "CV-Leonardo de Jesus Couto Pereira.pdf"

# Converter PDF para base64
base64_pdf = pdf_to_base64(pdf_path)

# Caminho para o arquivo de saída TXT
output_txt_file = "output.txt"

# Salvar a representação em base64 em um arquivo de texto
with open(output_txt_file, "w") as txt_file:
    txt_file.write(base64_pdf)

print("Representação em base64 do PDF salva em:", output_txt_file)